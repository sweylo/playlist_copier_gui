package PlaylistCopier;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.*;
import javax.swing.*;

public class MainGui extends JFrame {

    private static String mod;
    private static ArrayList<Track> tracks;
    private static boolean abort;
    
    public MainGui() {
        initComponents();
        mod = "main";
        disp("Idle");
        src.setText("C:\\Users\\Sweylo\\Desktop\\casual.m3u");
        dest.setText("E:playlist");
        //src.setText("C:\\Users\\Sweylo\\Documents\\casual.m3u");
        //dest.setText("D:\\playlist");
        ImageIcon icon = new ImageIcon("logo.png");
        setIconImage(icon.getImage());
        abort = false;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        srcLabel = new javax.swing.JLabel();
        src = new javax.swing.JTextField();
        destLabel = new javax.swing.JLabel();
        dest = new javax.swing.JTextField();
        runBttn = new javax.swing.JButton();
        status = new javax.swing.JTextField();
        progress = new javax.swing.JProgressBar();
        browseSrc = new javax.swing.JButton();
        browseDest = new javax.swing.JButton();
        bitrate_aac = new javax.swing.JSpinner();
        aacLabel = new javax.swing.JLabel();
        flacLabel = new javax.swing.JLabel();
        bitrate_flac = new javax.swing.JSpinner();
        overwrite = new javax.swing.JCheckBox();
        newPlaylist = new javax.swing.JCheckBox();
        abortBttn = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        optionsMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Playlist Copier");
        setLocationByPlatform(true);
        setResizable(false);

        srcLabel.setText("Source playlist file (.m3u or .m3u8)");

        destLabel.setText("Destination directory");

        dest.setToolTipText("");

        runBttn.setText("Run");
        runBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runBttnActionPerformed(evt);
            }
        });

        status.setEditable(false);

        browseSrc.setText("Browse");
        browseSrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseSrcActionPerformed(evt);
            }
        });

        browseDest.setText("Browse");
        browseDest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseDestActionPerformed(evt);
            }
        });

        bitrate_aac.setRequestFocusEnabled(false);
        bitrate_aac.setValue(256);

        aacLabel.setText("AAC Bitrate (kbps)");

        flacLabel.setText("FLAC Bitrate");

        bitrate_flac.setValue(256);

        overwrite.setSelected(true);
        overwrite.setText("Overwrite (if source newer)");

        newPlaylist.setText("Create new playlist file");
        newPlaylist.setEnabled(false);

        abortBttn.setForeground(new java.awt.Color(153, 0, 51));
        abortBttn.setText("Abort");
        abortBttn.setEnabled(false);
        abortBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abortBttnActionPerformed(evt);
            }
        });

        jMenu1.setText("Menu");

        optionsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_COMMA, java.awt.event.InputEvent.CTRL_MASK));
        optionsMenuItem.setText("Options");
        optionsMenuItem.setEnabled(false);
        optionsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(optionsMenuItem);

        quitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        quitMenuItem.setText("Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(quitMenuItem);

        jMenuBar1.add(jMenu1);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(destLabel)
                            .addComponent(srcLabel)
                            .addComponent(aacLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(bitrate_aac, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(flacLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bitrate_flac, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 216, Short.MAX_VALUE)
                                .addComponent(overwrite)
                                .addGap(18, 18, 18)
                                .addComponent(newPlaylist))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(src)
                                    .addComponent(dest))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(browseDest, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(browseSrc, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(runBttn))
                    .addComponent(progress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(status)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(abortBttn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(srcLabel)
                            .addComponent(src, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(browseSrc))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(destLabel)
                            .addComponent(dest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(browseDest))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bitrate_aac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(aacLabel)
                            .addComponent(flacLabel)
                            .addComponent(bitrate_flac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(overwrite)
                            .addComponent(newPlaylist)))
                    .addComponent(runBttn, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(progress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(abortBttn)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(status)))
                .addContainerGap())
        );

        setBounds(0, 0, 1079, 290);
    }// </editor-fold>//GEN-END:initComponents

    private void browseSrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseSrcActionPerformed
        final JFileChooser fc = new JFileChooser();
        if (fc.showOpenDialog(src) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            src.setText(file.getPath());
        }
    }//GEN-LAST:event_browseSrcActionPerformed

    private void browseDestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseDestActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fc.showOpenDialog(dest) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            dest.setText(file.getPath());
        }
    }//GEN-LAST:event_browseDestActionPerformed

    private void runBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runBttnActionPerformed
       
        Thread runButton = new Thread() {
            @Override
            public void run() {
                disp("Checking existence of source file...");
                // make sure the source field isn't empty
                if (src.getText().equals("")) {
                    error("No source file selected");
                } else {
                    // make sure the destination field isn't empty
                    if (dest.getText().equals("")) {
                        error("No destination selected");
                    } else {
                        Track playlist = new Track(src.getText());
                        File output = new File(dest.getText());
                        // make sure the source file can be read
                        if (playlist.canRead()) {
                            // make sure the source file has the proper extension
                            if (
                                playlist.getExtension().equals(".m3u") || 
                                playlist.getExtension().equals(".m3u8")
                            ) {
                                // make sure the output directory is found
                                if (output.canRead()) {
                                    disp("Reading playlist");
                                    tracks = Track.readPlaylist(playlist);
                                    for (Track track : tracks) {
                                        System.out.println(track.getName());
                                    }
                                    // make sure files are found in the playlist
                                    if (tracks.size() > 0) {
                                        disp("Successfully read playlist");
                                        itsMorphinTime(output);
                                    } else {
                                        error("No files found from the playlist");
                                    }
                                } else {
                                    error("Unable to locate destination directory");
                                }
                            } else {
                                error("Not the right type of file");
                            }
                        } else {
                            error("Unable to open playlist file");
                        }
                    }
                }
            }
        };
        
        runButton.start();
        
    }//GEN-LAST:event_runBttnActionPerformed

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void optionsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsMenuItemActionPerformed
        OptionsFrame opt = new OptionsFrame();
        opt.setVisible(true);
    }//GEN-LAST:event_optionsMenuItemActionPerformed

    private void abortBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_abortBttnActionPerformed
        abort = true;
        error("Aborting...");
    }//GEN-LAST:event_abortBttnActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        AboutFrame about = new AboutFrame();
        about.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed
    
    public static void main(String args[]) {
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (
            ClassNotFoundException | 
            InstantiationException | 
            IllegalAccessException | 
            UnsupportedLookAndFeelException ex
        ) {
            Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SwingUtilities.invokeLater(() -> {
            new MainGui().setVisible(true);
        });
        
    }
    
    // update status with status modifier and message
    private void disp(String s) {
        status.setForeground(Color.black);
        status.setText(String.format("[%s] - %s", mod, s));
    }
    
    // update status with error and error message
    private void error(String s) {
        status.setForeground(Color.red);
        status.setText(String.format("[error] - %s", s));
    }
    
    // Mastadon, Pteradactyl, Triceratops, Sabertooth Tiger, Tyrannosaurus
    private void itsMorphinTime(File output) {
        
        // lock interface to prevent parameters from changing
        runBttn.setEnabled(false);
        src.setEnabled(false);
        dest.setEnabled(false);
        browseSrc.setEnabled(false);
        browseDest.setEnabled(false);
        bitrate_aac.setEnabled(false);
        bitrate_flac.setEnabled(false);
        overwrite.setEnabled(false);
        abortBttn.setEnabled(true);
        
        System.out.println("Track list size: " + tracks.size());
        progress.setMaximum(tracks.size());
        int cores = Runtime.getRuntime().availableProcessors();
        
        for (int i = 0; i < tracks.size(); i++) {

            String processes = "";
            
            if (abort) {
                break;
            }
            
            Thread[] threads = new Thread[cores];
            
            for (int j = 0; j < cores; j++) {
                if (i < tracks.size() - 1) {
                    threads[j] = new ProcessTracks(
                        tracks.get(++i),
                        output, 
                        (Integer)bitrate_aac.getValue(), 
                        (Integer)bitrate_flac.getValue(),
                        overwrite.isSelected()
                    );
                    System.out.println("i = " + i + " | " + tracks.get(i).getName());
                    processes += tracks.get(i).getName() + ", ";
                    threads[j].start();
                } else {
                    break;
                }
            }
            
            disp("Processing: " + processes.substring(0, processes.lastIndexOf(",")));
            System.out.println("Processing: " + processes.substring(0, processes.lastIndexOf(",")));
            progress.setValue(i);
            this.setTitle(String.format("Playlist Copier - %d%%", 
                Math.round(((double) i / (double) tracks.size()) * 100)));
            
            for (Thread thread : threads) {
                if (thread != null) {
                    try {
                        thread.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
            /*Thread thread = new ProcessTracks(
                tracks.get(i), 
                output, 
                (Integer)bitrate_aac.getValue(), 
                (Integer)bitrate_flac.getValue(),
                overwrite.isSelected()
            );
            
            thread.start();
            
            try {
                thread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }*/
            
        }
        
        if (!abort) {
            disp("Finished");
        } else {
            error("Operation aborted");
            abort = false;
        }
        
        progress.setValue(0);
        this.setTitle("Playlist Copier");
        
        // unlock interface;
        runBttn.setEnabled(true);
        src.setEnabled(true);
        dest.setEnabled(true);
        browseSrc.setEnabled(true);
        browseDest.setEnabled(true);
        bitrate_aac.setEnabled(true);
        bitrate_flac.setEnabled(true);
        overwrite.setEnabled(true);
        abortBttn.setEnabled(false);
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel aacLabel;
    private javax.swing.JButton abortBttn;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JSpinner bitrate_aac;
    private javax.swing.JSpinner bitrate_flac;
    private javax.swing.JButton browseDest;
    private javax.swing.JButton browseSrc;
    private javax.swing.JTextField dest;
    private javax.swing.JLabel destLabel;
    private javax.swing.JLabel flacLabel;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JCheckBox newPlaylist;
    private javax.swing.JMenuItem optionsMenuItem;
    private javax.swing.JCheckBox overwrite;
    private javax.swing.JProgressBar progress;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JButton runBttn;
    private javax.swing.JTextField src;
    private javax.swing.JLabel srcLabel;
    private javax.swing.JTextField status;
    // End of variables declaration//GEN-END:variables
}

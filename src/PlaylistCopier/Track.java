package PlaylistCopier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Track extends File {

    public Track(String pathname) {
        super(pathname);
    }
    
    public String getExtension() {
        return getPath().substring(getPath().lastIndexOf("."));
    }
    
    public String getNameNoExtension() {
        return getName().substring(0, getName().lastIndexOf("."));
    }
    
    public static ArrayList<Track> readPlaylist(Track playlist) {
        
        ArrayList<Track> trackList = new ArrayList<>();
        
        try {

            FileInputStream input = new FileInputStream(playlist);
            char current = (char)input.read();
            String line = "";

            while(current < Character.MAX_VALUE) {
                
                if (current == 10 || current == 13) {   // check for either LF or CRLF line endings
                    
                    current = (char)input.read();
                    Track temp = new Track(line);
                    
                    if (temp.canRead() && temp.isAbsolute() && temp.isFile()) {
                        trackList.add(temp);
                    } else {
                        System.err.println("Unable to read " + temp.getPath());
                    }
                    
                    line = "";
                    
                    if (current == 10) {                // necessary for CRLF to catch the LF
                        current = (char)input.read();
                    }
                    
                } else {
                    //System.out.println(current + " | " + (int)current);
                    line += current;
                    current = (char)input.read();
                }
                
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return trackList;
        
    }
    
}

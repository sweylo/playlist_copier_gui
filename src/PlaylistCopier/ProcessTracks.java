package PlaylistCopier;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessTracks extends Thread {
    
    private final Track src;
    private final File dest;
    private final int aac;
    private final int flac;
    private final boolean ovrw;
    
    public ProcessTracks(Track src, File dest, int aac, int flac, boolean ovrw) {
        this.src = src;
        this.dest = dest;
        this.aac = aac;
        this.flac = flac;
        this.ovrw = ovrw;
    }
    
    @Override
    public void run () {
        
        String cmd = "";
        Runtime r = Runtime.getRuntime();
        int bitrate = 128;
        
        if (src.getExtension().equals(".m4a") || src.getExtension().equals(".mp3")) {
            bitrate = aac;
        } else if (src.getExtension().equals(".flac")) {
            bitrate = flac;
        }
        
        File outFile = new File(String.format("%s\\%s.mp3",
            dest.getPath(),
            src.getNameNoExtension()
        ));
        
        if (outFile.exists() && ovrw && (outFile.lastModified() > src.lastModified())) {
            return;
        }
        
        if (outFile.exists() && !ovrw) {
            return;
        }
        
        switch (src.getExtension()) {
            case ".mp3":
                /*cmd = String.format(
                    "cp \"%s\" \"%s\\%s\"", 
                    src.getPath(), 
                    dest.getPath(), 
                    src.getName()
                );
                break;*/
            case ".m4a":
            case ".flac":
                cmd = String.format(
                    "ffmpeg -%s -loglevel quiet -i \"%s\" -ab %dk -map_metadata 0 -id3v2_version 3 \"%s\\%s.mp3\"",
                    (ovrw) ? "y" : "n",
                    src.getPath(),
                    bitrate,
                    dest.getPath(),
                    src.getNameNoExtension()
                );
        }
        
        try {
            System.out.println(cmd);
            Process p = r.exec(cmd);
            p.waitFor();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
